package taskq

import (
	"fmt"
)

func Storage(queue Queue) string {
	return fmt.Sprintf("storage:%s", queue)
}

func Sets(queue Queue) string {
	return fmt.Sprintf("queue:%s", queue)
}

func Task(queue Queue) string {
	return fmt.Sprintf("task:%s", queue)
}

func Alive(namespace string, id string) string {
	return fmt.Sprintf("%s:alive:%s", namespace, id)
}

func Order(queue Queue) string {
	return fmt.Sprintf("order:%s", queue)
}
