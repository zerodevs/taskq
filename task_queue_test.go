package taskq

import (
	"testing"
	"context"
	"time"
	// "fmt"
	"encoding/json"
	"github.com/google/uuid"
	"github.com/gomodule/redigo/redis"
	"github.com/smallstep/assert"
)

func tryClaimShouldSucceed(
	t *testing.T,
	pool *redis.Pool,
	taskQueue *taskQueue,
	queue Queue,
	id MessageId,
	expected map[string]interface{},
)  {
	conn, err := pool.GetContext(context.Background())

	assert.Nil(t, err)
	assert.NotNil(t, conn)

	// test sending same id
	_, err = conn.Do(
		"sadd",
		Sets(queue),
		id,
	)

	assert.Nil(t, err)

	data := make(map[string]interface{})

	receivedId, receivedRawMessage, err := taskQueue.Dequeue(
		context.Background(),
		queue,
		&data,
		2 * time.Second,
	)

	assert.Nil(t, err)

	assert.NotNil(t, receivedRawMessage)
	assert.NotNil(t, receivedId)
	assert.Equals(t, id, receivedId)
	assert.Equals(t, expected, data)
}

func tryClaimShouldFailed(
	t *testing.T,
	pool *redis.Pool,
	taskQueue *taskQueue,
	queue Queue,
	id MessageId,
) {
	conn, err := pool.GetContext(context.Background())

	assert.Nil(t, err)
	assert.NotNil(t, conn)

	// test sending same id
	_, err = conn.Do(
		"sadd",
		Sets(queue),
		id,
	)

	assert.Nil(t, err)

	data := make(map[string]interface{})

	_, _, err = taskQueue.Dequeue(
		context.Background(),
		queue,
		&data,
		2 * time.Second,
	)

	assert.NotNil(t, err)

}

func TestExtendTask(t *testing.T) {

	pool := connectLocalhost(t)
	assert.NotNil(t, pool)

	message := map[string]interface{}{
		"test": "1",
	}

	taskQueue, err := NewTaskQueue(
		pool,
		json.Marshal, json.Unmarshal,
	)

	assert.Nil(t, err)
	assert.NotNil(t, taskQueue)

	queue := Queue("test")

	defer taskQueue.CleanupQueue(context.Background(), queue)

	id, err := taskQueue.Enqueue(
		context.Background(),
		queue,
		message,
	)

	assert.Nil(t, err)
	assert.NotNil(t, id)
	assert.NotEquals(t, uuid.UUID{}.String(), id)

	checkTaskExists(t, pool, queue, id, message)

	data := make(map[string]interface{})

	receivedId, rawMessage, err := taskQueue.Dequeue(
		context.Background(),
		queue,
		data,
		2 * time.Second,
	)

	assert.Nil(t, err)
	assert.NotNil(t, receivedId)
	assert.NotEquals(t, EmptyMessageId, receivedId)
	assert.NotNil(t, rawMessage)
	assert.NotEquals(t, 0, len(rawMessage))

	assert.Equals(t, id, receivedId)

	receivedMessage := make(map[string]interface{})

	assert.Nil(t, json.Unmarshal(rawMessage, &receivedMessage))

	for key, val := range message {
		assert.Equals(t, val, receivedMessage[key])
	}

	flag, err := taskQueue.Extend(
		context.Background(),
		queue,
		receivedId,
		2 * time.Second,
	)

	assert.Nil(t, err)
	assert.True(t, flag)

	tryClaimShouldFailed(t, pool, taskQueue, queue, receivedId)

	time.Sleep(4 * time.Second)

	tryClaimShouldSucceed(t, pool, taskQueue, queue, receivedId, receivedMessage)
}

func checkAckMessage(
	t *testing.T,
	pool *redis.Pool,
	taskQueue *taskQueue,
	queue Queue,
	id MessageId,
) {
	conn, err := pool.GetContext(context.Background())
	assert.Nil(t, err)
	assert.NotNil(t, conn)

	defer conn.Close()

	val, err := conn.Do(
		"zscore",
		Order(queue.Pending()),
		id,
	)

	assert.Nil(t, err)
	assert.Nil(t, val)

	args := []interface{}{
		Order(queue.Done()),
		id,
	}

	score, err := redis.Int(conn.Do(
		"zscore",
		args...,
	))

	assert.Nil(t, err)
	assert.True(t, score > 0)
}

func TestAckTask(
	t *testing.T,
) {

	pool := connectLocalhost(t)
	assert.NotNil(t, pool)

	message := map[string]interface{}{
		"test": "1",
	}

	taskQueue, err := NewTaskQueue(
		pool,
		json.Marshal, json.Unmarshal,
	)

	assert.Nil(t, err)
	assert.NotNil(t, taskQueue)

	queue := Queue("test")

	defer taskQueue.CleanupQueue(context.Background(), queue)

	id, err := taskQueue.Enqueue(
		context.Background(),
		queue,
		message,
	)

	assert.Nil(t, err)
	assert.NotNil(t, id)
	assert.NotEquals(t, uuid.UUID{}.String(), id)

	checkTaskExists(t, pool, queue, id, message)

	data := make(map[string]interface{})

	receivedId, rawMessage, err := taskQueue.Dequeue(
		context.Background(),
		queue,
		data,
		2 * time.Second,
	)

	assert.Nil(t, err)
	assert.NotNil(t, receivedId)
	assert.NotEquals(t, EmptyMessageId, receivedId)
	assert.NotNil(t, rawMessage)
	assert.NotEquals(t, 0, len(rawMessage))

	assert.Equals(t, id, receivedId)

	receivedMessage := make(map[string]interface{})

	assert.Nil(t, json.Unmarshal(rawMessage, &receivedMessage))

	err = taskQueue.Ack(
		context.Background(),
		queue,
		receivedId,
	)

	assert.Nil(t, err)

	checkAckMessage(
		t,
		pool,
		taskQueue,
		queue,
		receivedId,
	)
}


func checkNackMessage(
	t *testing.T,
	pool *redis.Pool,
	taskQueue *taskQueue,
	queue Queue,
	id MessageId,
) {
	conn, err := pool.GetContext(context.Background())
	assert.Nil(t, err)
	assert.NotNil(t, conn)

	defer conn.Close()

	val, err := conn.Do(
		"zscore",
		Order(queue.Pending()),
		id,
	)

	assert.Nil(t, err)
	assert.Equals(t, 0, val)

	args := []interface{}{
		Order(queue.Pending()),
		id,
	}

	score, err := redis.Int(conn.Do(
		"zscore",
		args...,
	))

	assert.Nil(t, err)
	assert.Equals(t, 0, score)
}


func TestNackTask(t *testing.T) {
	pool := connectLocalhost(t)
	assert.NotNil(t, pool)

	message := map[string]interface{}{
		"test": "1",
	}

	taskQueue, err := NewTaskQueue(
		pool,
		json.Marshal, json.Unmarshal,
	)

	assert.Nil(t, err)
	assert.NotNil(t, taskQueue)

	queue := Queue("test")

	defer taskQueue.CleanupQueue(context.Background(), queue)

	id, err := taskQueue.Enqueue(
		context.Background(),
		queue,
		message,
	)

	assert.Nil(t, err)
	assert.NotNil(t, id)
	assert.NotEquals(t, uuid.UUID{}.String(), id)

	checkTaskExists(t, pool, queue, id, message)

	data := make(map[string]interface{})

	receivedId, rawMessage, err := taskQueue.Dequeue(
		context.Background(),
		queue,
		data,
		2 * time.Second,
	)

	assert.Nil(t, err)
	assert.NotNil(t, receivedId)
	assert.NotEquals(t, EmptyMessageId, receivedId)
	assert.NotNil(t, rawMessage)
	assert.NotEquals(t, 0, len(rawMessage))

	assert.Equals(t, id, receivedId)

	receivedMessage := make(map[string]interface{})

	assert.Nil(t, json.Unmarshal(rawMessage, &receivedMessage))

	err = taskQueue.Nack(
		context.Background(),
		queue, receivedId,
	)

	assert.Nil(t, err)

}
