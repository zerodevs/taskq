package taskq

import (
	"sync"

	"github.com/prometheus/client_golang/prometheus"
)

var (
	once sync.Once

	// message rates
	MessageSummary = prometheus.NewGaugeVec(prometheus.GaugeOpts{
		Name: "taskq_queue_message_summary",
		Help: "taskq library message summary status",
	}, []string{"queue", "state"})

	// queue rates
	QueueSummary = prometheus.NewGaugeVec(prometheus.GaugeOpts{
		Name: "taskq_queue_queue_summary",
		Help: "taskq library queue summary status",
	}, []string{"queue", "state", "status"})

	// queue taskq operation summary
	OperationSummary = prometheus.NewHistogramVec(prometheus.HistogramOpts{
		Name: "taskq_operation_summary",
		Help: "taskq library operation summary stats",
		Buckets: prometheus.ExponentialBuckets(2, 2, 15),
	}, []string{"queue", "action", "status"})

)

func init() {
	once.Do(func() {
		prometheus.MustRegister(
			MessageSummary,
			QueueSummary,
			OperationSummary,
		)
	})
}
