package taskq

import (
	"context"
	"encoding/json"
	"fmt"
	"math/rand"
	"strconv"
	// "sync"
	"sync/atomic"
	"testing"
	"time"

	// "github.com/gomodule/redigo/redis"
	"github.com/smallstep/assert"
)

// Try enqueue several times while also
// try to do dequeue in different go routine
// k publisher sends N messages and m consumer
//
func TestSimpleEnqueueAndDequeueThreaded(t *testing.T) {
	pool := connectLocalhost(t)
	assert.NotNil(t, pool)

	queue := Queue("test_threaded")

	taskQueue, err := NewTaskQueue(
		pool,
		json.Marshal,
		json.Unmarshal,
	)

	assert.Nil(t, err)

	N := 10

	messagesDone := make([]int, N)

	// send multiple random messages
	for ii := 0; ii < N; ii++ {
		// in minutes
		// the total amount of duration, a task need
		//
		taskDuration := (rand.Int() * ii) % 5

		message := map[string]interface{}{
			"index" : fmt.Sprintf("%d", ii),
			"duration" : fmt.Sprintf("%d", taskDuration),
			"content": fmt.Sprintf("content %d", ii),
		}

		go func() {
			lid, lerr := taskQueue.Enqueue(
				context.Background(),
				queue,
				message,
			)

			assert.Nil(t, lerr)
			assert.NotNil(t, lid)
			assert.NotEquals(t, EmptyMessageId, lid)
		}()
	}

	// receive multiple random messages
	var receivedMessageCounter uint64 = 0
	counter := 0

	for int(receivedMessageCounter) < N {
		receivedMessage := make(map[string]string)

		go func() {
			// random sleep to imitate random wake
			time.Sleep(2 * time.Second)

			workerDuration := 3 * time.Minute

			receivedId, rawMessage, err := taskQueue.Dequeue(
				context.Background(),
				queue,
				&receivedMessage,
				workerDuration,
			)

			if err == nil {
				assert.NotNil(t, receivedId)
				assert.NotEquals(t, EmptyMessageId, receivedId)
				assert.NotNil(t, rawMessage)
				assert.True(t, len(rawMessage) > 0)

				// fmt.Println(receivedMessage)

				taskIntDuration, err := strconv.Atoi(receivedMessage["duration"])
				assert.Nil(t, err)

				// fmt.Printf("type: %T\n", receivedMessage["index"])
				messageIndex, err := strconv.Atoi(receivedMessage["index"])
				assert.Nil(t, err)

				taskDuration := time.Duration(taskIntDuration) * time.Minute
				extendDuration := taskDuration.Truncate(workerDuration)

				flag, err := taskQueue.Extend(
					context.Background(),
					queue,
					receivedId,
					extendDuration,
				)

				assert.Nil(t, err)
				assert.True(t, flag)

				time.Sleep(extendDuration)

				assert.Nil(t, taskQueue.Ack(
					context.Background(),
					queue,
					receivedId,
				))

				_ = atomic.AddUint64(&receivedMessageCounter, 1)

				messagesDone[messageIndex] += 1
			}

		}()

		if counter % 4 == 0 {
			time.Sleep(2 * time.Second)
		}

		counter += 1
	}

}
