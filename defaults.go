package taskq

import (
	"github.com/google/uuid"
	"crypto/rand"
)

const (
	EmptyMessageId = ""
)

func DefaultSeqGenerator(_ RawMessage) (string, error) {
	id, err := uuid.NewRandomFromReader(rand.Reader)

	if err != nil {
		return "", err
	}

	return id.String(), nil
}
