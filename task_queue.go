package taskq

import (
	"context"
	// "fmt"
	"time"
)


func (q *taskQueue) Extend(
	ctx context.Context,
	queue Queue,
	id MessageId,
	duration time.Duration,
) (bool, error) {

	conn, err := q.pool.GetContext(ctx)

	if err != nil {
		return false, err
	}

	resp, err := conn.Do(
		"expire",
		Alive(Task(queue), string(id)),
		duration.Seconds(),
	)

	if err != nil {
		return false, err
	}

	return resp != nil, nil
}

func (q *taskQueue) Ack(
	ctx context.Context,
	queue Queue,
	id MessageId,
) error {

	conn, err := q.pool.GetContext(ctx)

	if err != nil {
		return err
	}

	defer conn.Close()

	// remove from pending

	_, err = conn.Do(
		"zrem",
		Order(queue.Pending()),
		id,
	)

	if err != nil {
		return err
	}

	// add to done
	_, err = conn.Do(
		"zadd",
		Order(queue.Done()),
		time.Now().Second(),
		id,
	)

	return err
}

func (q *taskQueue) Nack(
	ctx context.Context,
	queue Queue,
	id MessageId,
) error {
		conn, err := q.pool.GetContext(ctx)

	if err != nil {
		return err
	}

	defer conn.Close()

	// remove from pending
	err = conn.Send(
		"zrem",
		Order(queue.Pending()),
		id,
	)

	if err != nil {
		return err
	}

	// add to done
	err = conn.Send(
		"sadd",
		Sets(queue),
		id,
	)

	if err != nil {
		return err
	}

	return conn.Flush()
}
