package taskq

func Status(err error) string {
	if err != nil {
		return "error.any"
	}

	return "ok"
}
