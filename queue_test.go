package taskq

import (
	"testing"
	"context"
	// "fmt"
	"time"
	"encoding/json"
	"github.com/google/uuid"
	"github.com/smallstep/assert"
	"github.com/gomodule/redigo/redis"
)

func connectLocalhost(t *testing.T) *redis.Pool {
	pool := &redis.Pool{
		MaxActive:       10,
		Wait:            true,
		MaxConnLifetime: 2 * time.Second,
		MaxIdle:         10,
		Dial: func() (redis.Conn, error) {
			conn, err := redis.Dial("tcp", ":6379")

			if err != nil {
				return nil, err
			}

			return conn, nil
		},
	}

	conn := pool.Get()
	defer conn.Close()

	_, err := conn.Do("PING")

	assert.Nil(t, err)

	return pool
}

func checkTaskExists(
	t *testing.T,
	pool *redis.Pool,
	queue Queue,
	id MessageId,
	raw interface{},
) {

	conn, err := pool.GetContext(context.Background())

	assert.Nil(t, err)

	defer conn.Close()

	flag, err := redis.Bool(conn.Do(
		"hexists",
		Storage(queue),
		id,
	))

	assert.Nil(t, err)
	assert.True(t, flag)

	flag, err = redis.Bool(conn.Do(
		"sismember",
		Sets(queue),
		id,
	))

	assert.Nil(t, err)
	assert.True(t, flag)
}


func TestEnqueueTask(t *testing.T) {
	pool := connectLocalhost(t)
	assert.NotNil(t, pool)

	message := map[string]interface{}{
		"test": "1",
	}

	taskQueue, err := NewTaskQueue(
		pool,
		json.Marshal, json.Unmarshal,
	)

	assert.Nil(t, err)
	assert.NotNil(t, taskQueue)

	queue := Queue("test")

	defer taskQueue.CleanupQueue(context.Background(), queue)

	id, err := taskQueue.Enqueue(
		context.Background(),
		queue,
		message,
	)

	assert.Nil(t, err)
	assert.NotNil(t, id)
	assert.NotEquals(t, uuid.UUID{}.String(), id)

	checkTaskExists(t, pool, queue, id, message)
}

func TestDequeueTask(t *testing.T) {
	pool := connectLocalhost(t)
	assert.NotNil(t, pool)

	message := map[string]interface{}{
		"test": "1",
	}

	taskQueue, err := NewTaskQueue(
		pool,
		json.Marshal, json.Unmarshal,
	)

	assert.Nil(t, err)
	assert.NotNil(t, taskQueue)

	queue := Queue("test")

	defer taskQueue.CleanupQueue(context.Background(), queue)

	id, err := taskQueue.Enqueue(
		context.Background(),
		queue,
		message,
	)

	assert.Nil(t, err)
	assert.NotNil(t, id)
	assert.NotEquals(t, uuid.UUID{}.String(), id)

	checkTaskExists(t, pool, queue, id, message)

	data := make(map[string]interface{})

	receivedId, rawMessage, err := taskQueue.Dequeue(
		context.Background(),
		queue,
		data,
		2 * time.Second,
	)

	assert.Nil(t, err)
	assert.NotNil(t, receivedId)
	assert.NotEquals(t, EmptyMessageId, receivedId)
	assert.NotNil(t, rawMessage)
	assert.NotEquals(t, 0, len(rawMessage))

	// fmt.Println("id", id, "receivedId", receivedId)

	assert.Equals(t, id, receivedId)

	receivedMessage := make(map[string]interface{})

	assert.Nil(t, json.Unmarshal(rawMessage, &receivedMessage))

	for key, val := range message {
		assert.Equals(t, val, receivedMessage[key])
	}

}
