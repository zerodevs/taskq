package taskq

import (
	"context"
	"fmt"
	"time"
)

type MessageId string

type Marshaler func(interface{}) ([]byte, error)
type Unmarshaler func([]byte, interface{}) error

type RawMessage map[string]interface{}
type Queue string

func (q Queue) Alive() Queue {
	return Queue(fmt.Sprintf("%s:%s", q, "alive"))
}

func (q Queue) Done() Queue {
	return Queue(fmt.Sprintf("%s:%s", q, "done"))
}

func (q Queue) Pending() Queue {
	return Queue(fmt.Sprintf("%s:%s", q, "pending"))
}

func (q Queue) Deadletter() Queue {
	return Queue(fmt.Sprintf("%s:%s", q, "deadletter"))
}

type SeqGenerator func(RawMessage) (string, error)

type MessageQueue interface {
	Enqueue(
		context.Context,
		Queue,
		RawMessage,
	) (MessageId, error)

	Enqueue2(
		context.Context,
		SeqGenerator,
		Queue,
		RawMessage,
	) (MessageId, error)

	Dequeue(
		context.Context,
		Queue,
		interface{},
		time.Duration,
	) (MessageId, []byte, error)
}

type TaskQueue interface {
	MessageQueue
	Ack(context.Context, Queue, MessageId) error
	Nack(context.Context, Queue, MessageId) error
	Extend(context.Context, Queue, MessageId, time.Duration) (bool, error)
}

type StealQueue interface {
	TryClaim(context.Context, Queue) error
}

type AdminQueue interface {
	// lookup all pending message above given duration
	ReclaimPending(context.Context, Queue, time.Duration) error

	// cleanup given queue
	CleanupQueue(context.Context, Queue) error
}

type MonitorQueue interface {
	PendingMessages(context.Context, Queue) (uint, error)
}
