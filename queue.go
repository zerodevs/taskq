package taskq

import (
	"context"
	"crypto/rand"
	"time"
	"fmt"
	"github.com/gomodule/redigo/redis"
	"github.com/google/uuid"
)

type taskQueue struct {
	pool *redis.Pool
	marshaler Marshaler
	unmarshaller Unmarshaler
}

func NewTaskQueue(
	pool *redis.Pool,
	marshaler Marshaler,
	unmarshaler Unmarshaler,
) (*taskQueue, error) {
	return &taskQueue{
		pool,
		marshaler,
		unmarshaler,
	}, nil
}

func (t *taskQueue) CleanupQueue(
	ctx context.Context,
	queue Queue,
) error {

	conn, err := t.pool.GetContext(ctx)

	if err != nil {
		return err
	}

	_, err = conn.Do(
		"del",
		Storage(queue),
		Sets(queue),
		Task(queue),
		Order(queue.Pending()),
	)

	if err != nil {
		return err
	}

	return err

}

func (t *taskQueue) Enqueue(
	ctx context.Context,
	queue Queue,
	message RawMessage,
) (id MessageId, err error) {
	return t.Enqueue2(ctx, DefaultSeqGenerator, queue, message)
}

func (t *taskQueue) Enqueue2(
	ctx context.Context,
	seqg SeqGenerator,
	queue Queue,
	message RawMessage,
) (id MessageId, err error) {

	begin := time.Now()

	defer func() {
		OperationSummary.
			WithLabelValues(string(queue), "enqueue", Status(err)).
			Observe(float64(time.Since(begin).Milliseconds()))

		if err == nil {
			MessageSummary.WithLabelValues(string(queue), "queue").Inc()
		}
	}()

	conn, err := t.pool.GetContext(ctx)

	if err != nil {
		return EmptyMessageId, err
	}

	defer conn.Close()

	raw, err := t.marshaler(message)

	if err != nil {
		return EmptyMessageId, err
	}

	rawId, err := uuid.NewRandomFromReader(rand.Reader)

	if err != nil {
		return EmptyMessageId, err
	}

	id = MessageId(rawId.String())

	if err = conn.Send(
		"hset",
		Storage(queue),
		id,
		raw,
	); err != nil {
		return id, err
	}

	if err = conn.Send(
		"sadd",
		Sets(queue),
		id,
	); err != nil {
		return id, err
	}

	if err = conn.Flush(); err != nil {
		return id, err
	}

	return id, nil
}


func (t *taskQueue) Dequeue(
	ctx context.Context,
	queue Queue,
	data interface{},
	duration time.Duration,
) (id MessageId, raw []byte, err error) {

	begin := time.Now()

	defer func() {
		OperationSummary.
			WithLabelValues(string(queue), "enqueue", Status(err)).
			Observe(float64(time.Since(begin).Milliseconds()))

		if err == nil {
			MessageSummary.WithLabelValues(string(queue), "queue").Dec()
			MessageSummary.WithLabelValues(string(queue), "inflight").Inc()
		}
	}()

	conn, err := t.pool.GetContext(ctx)

	if err != nil {
		return EmptyMessageId, nil, err
	}

	defer conn.Close()

	rawId, err := redis.String(conn.Do("spop", Sets(queue)))

	if err != nil {
		return EmptyMessageId, nil, err
	}

	_, err = uuid.Parse(rawId)

	if err != nil {
		return EmptyMessageId, nil, err
	}

	id = MessageId(rawId)

	resp, err := conn.Do(
		"set",
		Alive(Task(queue), rawId),
		id,
		"ex",
		duration.Seconds(),
		"nx",
	)

	if err != nil {
		return id, nil, err
	}

	if resp == nil {
		return id, nil, fmt.Errorf("can't reserve the task %s for queue %s", id, queue)
	}

	raw, err = redis.Bytes(
		conn.Do("hget", Storage(queue), id),
	)

	if err != nil {
		return id, nil, err
	}

	err = t.unmarshaller(raw, &data)

	if err != nil {
		return id, raw, err
	}

	resp, err = conn.Do(
		"zadd",
		Order(queue.Pending()),
		"nx",
		time.Now(),
		id,
	)

	if resp == nil {
		return id, nil, fmt.Errorf("can't add the task %s for pending queue of %s", id, queue)
	}

	return id, raw, nil
}
